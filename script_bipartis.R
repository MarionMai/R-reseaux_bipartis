
## ---------------------------------------------------
library("igraph")
library("tnet")


## ------------------------------------------------------------------------

#ouvrir le fichier contenant la matrice
mat<-read.csv(file="freeman_friendship.csv", header=T, sep=";")

#remplacer les cases vides par des zéros
mat<-as.matrix(mat[,2:20])

mat[is.na(mat)] = 0

#transformer la matrice en objet igraph
g<-graph_from_incidence_matrix(as.matrix(mat), directed = FALSE, multiple = FALSE, 
                            weighted = NULL, add.names = NULL)
g


## ------------------------------------------------------------------------

#l'attribut "type" permet de différencier les deux ensembles de sommets

#numéroter les membres du réseau

V(g)[V(g)$type == 0]$name<-1:29

#numéroter les évènements

V(g)[V(g)$type == 1]$name<-paste("E",1:19, sep="" )

vertex_attr(g, "label") <- V(g)$name


## ------------------------------------------------------------------------

#symboliser les évènements par des carrés et les individus par des cercles

V(g)[V(g)$type == 1]$shape <- "square"
V(g)[V(g)$type == 0]$shape <- "circle"

#choisir deux gammes de couleur pour bien distinguer les deux ensembles de sommets

col <- c("steelblue", "orange")

#suppression des isolés
g1 <- delete.vertices(g, V(g)[degree(g)==0])

plot(g1,
  vertex.color = col[as.numeric(V(g1)$type)+1],
  vertex.label.cex=0.6,
  main= "Réseaux acteurs-évènements",
  sub= "Les isolés ne sont pas représentés"
  ,layout=layout.fruchterman.reingold(g1, niter=10000) #layout_as_bipartite
)


## ------------------------------------------------------------------------

#mesurer le degré de l'ensemble des sommets
V(g)$degre <- degree(g)

#degré des évènements : combien d'individus ont participé à chaque évènement ?
V(g)[V(g)$type == 1]$degre


## ------------------------------------------------------------------------
#degré des individus : à combien d'évènements ont participé chaque individu ?
V(g)[V(g)$type == 0]$degre


## ------------------------------------------------------------------------

#extraire la liste des composantes connexes
comp <- components(g)

#repérer l'identifiant de la composante principale

cp <- which(comp$csize %in% max(comp$csize))

#associer aux sommets du graphe, le vecteur d'appartenance à une composante 

V(g)$membership<-comp$membership

#compter le nombre d'individus dans la composante principale
vcount(induced.subgraph(g,V(g)[V(g)$type == 0 & V(g)$membership == cp]))

#compter le nombre d'évènements dans la composante principale
vcount(induced.subgraph(g,V(g)[V(g)$type == 1 & V(g)$membership == cp]))


## ----------------

#transformer le graphe en liste de liens pour appliquer les fonctions du package tnet

net<-get.edgelist(g,names=F)

# calculer la matrice des plus courts chemins entre individus

mat_pcc<-distance_tm(as.tnet(net), projection.method="sum")


## ------------------------------------------------------------------------

#trouver la longueur du plus long des plus courts chemins

max(mat_pcc[,1:21], na.rm=T)


## ------------------------------------------------------------------------

#différentes méthodes de projection

#obtenir le réseau des individus non-valué

onemode1<-projecting_tm(net, method="binary")

#obtenir le réseau des individus valué en fonction du nombre d'évènements partagés

onemode2<-projecting_tm(net, method="sum")

#obtenir le réseau des individus valué en fonction du nombre d'évènements partagés 
#et pondéré en fonction du nombre de participants à chaque évènement
#l'hypothèse sous-jacente étant que moins il y a de monde dans un évènement, 
#plus il y a de chance que les participants se soient parlés

onemode3<-projecting_tm(net, method="Newman")


## ------------------------------------------------------------------------

#différents indices de centralité

#calcul du degré des individus

degree_tm <- degree_tm(net, measure="degree")

#calcul du coefficient de reinforcement

reinforcement_tm(net)

#calcul du clustering coefficient

clustering_tm(net)



## ------------------------------------------------------------------------
net2 <- net[,2:1]



## ---------------------------------------------------
library("bipartite")

#ouvrir le fichier contenant la matrice
mat<-read.csv(file="freeman_friendship.csv", header=T, sep=";")

#transformer en objet de type matrice
mat<-as.matrix(mat[1:29,2:20])

#renommer les colonnes (évènements)
colnames(mat)<- paste("E",1:19, sep="" )

#remplacer les cases vides par des zéros
mat[is.na(mat)] = 0

#représenter l'information
plotweb(mat)



## ------------------------------------------------------------------------

#mesures
networklevel(mat)



## ------------------------------------------------------------------------

#mesures
indices_link<-linklevel(mat)



## ------------------------------------------------------------------------

#calculer le degré normalisé
ND(mat, normalised = T )



## ---------------------------------------------------

#modèle nul
modele_nul<-mgen(mat)

modele_nul<-nullmodel(N=5, mat, method="mgen")



## ------------------------------------------------------------------------

#trouver des partitions
mod_mat<-computeModules(mat)

plotModuleWeb(mod_mat)

