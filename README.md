Le tutoriel est accessible ici : [https://marionmai.frama.io/R-reseaux_bipartis/](https://marionmai.frama.io/R-reseaux_bipartis/)

Le projet est composé de :

* L'article de Linton C. Freeman, 1980, _Int. J. Man-Machine Studies_
* Du jeu de données tiré de cet article
* Du code R permettant d'analyser ce jeu de données
* D'une liste de liens utiles pour l'analyse de réseaux bipartis
* D'un diaporama pdf
* D'un document pdf et _markdown_ (md) détaillant tout le contenu du cours
 * Définition d'un réseau biparti
 * Indicateurs
 * Projections
 * Visualisation


