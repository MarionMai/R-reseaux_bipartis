# Réseaux bipartis : inventaire des données disponibles en ligne

* [_Southern Women Data Set_. L'exemple canonique !](https://networkdata.ics.uci.edu/netdata/html/davis.html)
* [Autres jeux de données de l'Université de Californie à Irvine. _The UCI Network Data Repository_] (https://networkdata.ics.uci.edu/)
* [Jeux de données stockés par Jure Leskovec, Université de Stanford] (https://snap.stanford.edu/data/links.html)
* [La localisation des institutions de l'ONU. _GaWC datasets_] (http://www.lboro.ac.uk/gawc/datasets/da23.html)
* [_Legislative Cosponsorship Networks in the U.S. House and Senate_] (http://jhfowler.ucsd.edu/cosponsorship.htm)
* [_Neo4j datasets_] (https://neo4j.com/developer/example-data/#_data_sets)
* [_Neo4j datasets_. L'exemple des _Panama Papers_] (https://neo4j.com/blog/analyzing-panama-papers-neo4j/)
* [Données de citations d'articles] (http://www.cs.cornell.edu/projects/kddcup/datasets.html)
* [Jeux de données Pajek] (http://vlado.fmf.uni-lj.si/pub/networks/data/)
* [Encore plus de réseaux ! Les liens repérés par Mark Newman] (http://www-personal.umich.edu/~mejn/netdata/)
* [_Boards and gender_, un exemple de Tore Opshal et Cathrine Seierstad](http://boardsandgender.com/index.php)

# Réseaux bipartis : tutoriels

* [_Working with Bipartite/Affiliation Network Data in R_. Tutoriel de Solomon Messing](https://solomonmessing.wordpress.com/2012/09/30/working-with-bipartiteaffiliation-network-data-in-r/)
* [Manuel du groupe d'analyse de réseaux sociaux sous R de l'Université de Stanford](http://sna.stanford.edu/rlabs.php)
* [Représenter les réseaux bipartis sous R. 1](https://rstudio-pubs-static.s3.amazonaws.com/193179_da1331536a054b69b44a1846faa2c5a7.html)
* [Représenter les réseaux bipartis sous R. 2](https://www.sixhat.net/r-and-igraph-colouring-community-nodes-by-attributes.html)
* [Représenter les réseaux bipartis sous R. 3](http://stackoverflow.com/questions/31366066/how-to-plot-a-bipartite-graph-in-r)
* [Charger un graphe biparti avec le package R _igraph_](https://rpubs.com/lgadar/load-bipartite-graph)
* [Documentation du package R _bipartite_](https://cran.r-project.org/web/packages/bipartite/bipartite.pdf)
* [Tutoriel pour le package R _bipartite_ par Laurent Beauguitte](https://arshs.hypotheses.org/260)
* [Documentation du package R tnet de Tore Opsahl](https://cran.r-project.org/web/packages/tnet/tnet.pdf)
* [Tutoriel de Tore Opsahl. Package _R tnet_](https://toreopsahl.com/tnet/two-mode-networks/defining-two-mode-networks/)
* [Tutoriel de Tore Opsahl. Définir un réseau biparti](https://toreopsahl.com/tnet/two-mode-networks/defining-two-mode-networks/)
* [Tutoriel de Tore Opsahl. Les données d'un réseau biparti](https://toreopsahl.com/tnet/software/two-mode-data-structure/)
* [Tutoriel de Tore Opsahl. Mesurer le plus court chemin dans un réseau biparti](https://toreopsahl.com/tnet/two-mode-networks/shortest-paths/)
* [Tutoriel de Tore Opsahl. La projection d'un réseau biparti en réseau _one-mode_](https://toreopsahl.com/tnet/two-mode-networks/projection/)
* [Visualisation de réseaux bipartis sous Gephi. Tutoriel de Martin Grandjean](http://www.martingrandjean.ch/gephi-introduction/)
* [Article de Stephen P. Borgatti et Martin G. Everett. Concepteurs de Ucinet](http://www.analytictech.com/borgatti/2mode.htm)
* [Analyser des grands réseaux avec SNAP. Tutoriel de Jure Leskovec] (https://snap.stanford.edu/index.html)
* [Analyser et visualiser des réseaux avec Neo4j] (https://neo4j.com)


